#!/bin/bash
nvprof --print-gpu-summary ./vecmatmult  2> time.dat
echo " "
echo "Times: "
grep -o -P '.{0,11}calc_z_colwise' time.dat 
grep -o -P '.{0,11}calc_z_rowwise' time.dat 
rm time.dat
