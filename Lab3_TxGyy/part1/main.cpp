#include <stdio.h>
#include <iostream>
#include <math.h>
#include "vecmatmult.h"

int main() {

	//Model input
	double** X;
	double* y;
	double* yhat;
	int M;
	int N;

	//Model parameters
	double* w;
	double b=0.5;

	//Creating the model
	FILE *fpx;
	fpx=fopen("../data/train_x.dat","r");
	int mm;
	fscanf(fpx," %d\n",&mm);

	int *x_train = (int*)malloc(mm*sizeof(int));
	for(int t=0;t<mm;t++)
		fscanf(fpx," %d\n",&x_train[t]);

	FILE *fpy;
	fpy=fopen("../data/train_y.dat","r");
	int nn;
	fscanf(fpy," %d\n",&nn);

	int *y_train = (int*)malloc(nn*sizeof(int));
	for(int t=0;t<nn;t++)
		fscanf(fpy," %d\n",&y_train[t]);


	//Allocating, scaling and ordering the data
	N=nn;
	M=mm/N;

	//Allocating the model input, output and parameters
	w = (double *) malloc( M*sizeof(double));
	X = (double **)malloc( N * sizeof(double *));
	for (int i = 0; i< N; i++)
		X[i] = (double *) malloc ( M * sizeof(double));

	y = (double *) malloc( N * sizeof(double));
	yhat = (double *) malloc( N * sizeof(double));

	int count=0;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			X[i][j]=(double) x_train[count]/255.0;
			count++;
		}
		y[i]=(double)y_train[i];
	}

	free(x_train);
	free(y_train);
	
	printf("Model created\n");
	printf("N: %d\n",N);
	printf("M: %d\n",M);



	//Creating  w and b
	initialize_test(w, b, N, M);

	make_comparison(w, b, X, N, M);


	//Deallocating
	free(w);
	for (int i=0; i<N; i++)
		free (X[i]);

	free(X);
	free(y);
	free(yhat);

	return 0;

}
