#ifndef REGRESSION_H
#define REGRESSION_H

void optimize(double *w, double b,  double **X, double *y, double *yhat, int N, int M);

#endif
