#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "vecmatmult.h"

// To Do: Copy your macro from exercise 1 or create a more adeaquate

void initialize_test(double *w, double &b, int N, int M){

    for(int i = 0; i < M; i++)
        w[i]=sin(1.0*i) + sin(5.0*i) + sin(10.0*i);
    b=0.5;
}

void verify(double *z1, int N)
{
    double diff1,maxdiff=0.0;
  
    double *z0;

    z0= (double *) malloc( N * sizeof(double));

    FILE *fp;
    fp=fopen("../data/sol.dat","r");
    
    for(int j=0;j<N;j++)
	fscanf(fp," %lf \n",&z0[j]);

    fclose(fp);

    for(int i=0;i<N;i++)
    {
        diff1=fabs(z1[i]-z0[i])/fabs(z0[i]);
        
        if(diff1>maxdiff) maxdiff=diff1;

        if(diff1>0.01) printf("i %d: cuda: %f sol:%f \n",i,z1[i],z0[i]);
    }
    printf("Error: %f%\n",maxdiff*100);

}



// Copy your convertion from exercise 1 or create a new one more adeaquate to your implementation
void convertXtoX1D(double *X1D, double **X, int N, int  M)
{
    // To Do:
}


/* CUDA kernel
Remeber some things that help the implementation:
 - The number of threads is a power of  2
 - M is divisible by the number of threads requested 
 - Take a look at the compilation info provided by PGI in exercise 1 */
__global__ void cucalc_z(int n,int m, double* d_w, double* d_z, double* d_X1D, double b )
{
   //To Do:

}




void calc_z_cuda(double * __restrict__ z1, double* __restrict__ w, double* __restrict__ X1D, double b, int N, int M){

   //To Do:

   //arrays to be used by cuda kernel 
   double *d_w;
   double *d_z;
   double *d_X1D;


   //To Do: Allocate arrays in GPU


   //To Do: Transfer data to GPU


   //To Do: Calculate threads and blocks
   int threads;
   int blocks;



   //Create the launching grids, you can change this to try a more complex configuration 
   dim3 gridBlocks(blocks,1,1);
   dim3 gridThreads(threads,1,1);


   //To Do: Execute in GPU.  You can change this line if you want to try a more complex launch
   cucalc_z<<<gridBlocks,gridThreads>>>(N, M, d_w, d_z, d_X1D ,b);


   //To Do: Transfer result back

 
   //Free arrays in GPU
   cudaFree(d_w);
   cudaFree(d_z);
   cudaFree(d_X1D);
}



void make_comparison(double *w, double b,  double **X, int N, int M)
{

    double* z1;

    z1=(double*) malloc(N*sizeof(double));

    double *X1D;
    X1D = (double*) malloc(N*M*sizeof(double));
 
    convertXtoX1D(X1D,X,N,M);

    calc_z_cuda(z1, w, X1D, b, N, M);

    verify(z1, N);


    free(z1);
    free(X1D);
}

