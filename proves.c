#include <stdio.h>
#include <stdlib.h>

void convertXtoX1D_rows(double **X, double *X1D_rows, int N, int M)
{
    for(int i=0;i<N;i++){
        for(int j=0;j<M;j++){
            X1D_rows[j+i*M] = X[i][j]; 
        }
    }
    // printf("%f\n",X[0][0]);   
}

void calc_z_colwise(double * __restrict__ z1, double* __restrict__ w, double b, double * __restrict__ X1D_cols,int N, int M )
{
    for(int i=0;i<N;i++){
        double sum = 0.0;
        for(int j=0;j<M;j++){
            sum += X1D_cols[i*M+j] * w[j]; //i*M+j
        }
        z1[i] = sum + b;
    }
}

int main()
{
    int n=2;
    int m=3;
    // int X[2][3]={{1,2,3},{4,5,6}};
    double** X = (double **)malloc( n * sizeof(double *));
	for (int i = 0; i< n; i++)
		X[i] = (double *) malloc ( m * sizeof(double));

    double* X_r = (double*) malloc(n*m*sizeof(double)); 
    X[0][0] = 0.0;
    X[0][1] = 1.0;
    X[0][2] = 2.0;
    X[1][0] = 3.0;
    X[1][1] = 4.0;
    X[1][2] = 5.0;
    // printf("%d\n",X);
    convertXtoX1D_rows(X,X_r,n,m);
    double b=0.0;
    double* z1 = (double *)malloc( n *m* sizeof(double *));
    double* w = (double *)malloc( m * sizeof(double *));
    w[0] = 2;
    w[1] = 3;
    w[2] = 4;
    calc_z_colwise(z1, w, b, X_r, n, m);

    for (int i = 0; i < n; i++)    {
        for (int j = 0; j < m; j++)        {
            // *(*(matrix + i) + j) is equivalent to matrix[i][j]
            printf("%f ", X[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    for (int i = 0; i < n*m; i++)    {
      
            printf("%f ", X_r[i]);
       
       
    }

    printf("\n");

    for (int i = 0; i < n; i++)    {
      
            printf("%f ", z1[i]);
       
       
    }
   
    return 0;
}