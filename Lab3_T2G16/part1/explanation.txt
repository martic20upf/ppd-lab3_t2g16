La implementació row-wise és més òptima ja que al tractar-se d'una multiplicació d'una matriu per 
un vector fa què l'accés a memòria sigui amb més localitat a diferència de la col-wise on només per a fer 
la multiplicació de la primera fila pel vector has de recórrer tot el vector col-wise per aconseguir el 
primer element de cada columna necessaris per la multiplicació amb el vector es per aixó que (en especial 
per matrius molt grans) la cache no pot ajudar i per tant l'accés a memòria és molt menys óptim.
