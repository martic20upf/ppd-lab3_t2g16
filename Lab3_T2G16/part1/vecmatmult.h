#ifndef VECMATMULT_H
#define VECMATMULT_H

void initialize_test(double *w, double &b, int N, int M);
void make_comparison(double *w, double b, double **X, int N, int M);

#endif
