#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "vecmatmult.h"

//To do: (Macro)
#define INDEX_COL i+j*N
#define INDEX_ROW j+i*M


void initialize_test(double *w, double &b, int N, int M){

    for(int i = 0; i < M; i++)
        w[i]=sin(1.0*i) + sin(5.0*i) + sin(10.0*i);
    b=0.5;
}

void verify(double *z1, double *z2, int N)
{
    double diff1,diff2,maxdiff=0.0;
  
    double *z0;

    z0= (double *) malloc( N * sizeof(double));

    FILE *fp;
    fp=fopen("../data/sol.dat","r");
    
    for(int j=0;j<N;j++)
	fscanf(fp," %lf \n",&z0[j]);

    fclose(fp);

    for(int i=0;i<N;i++)
    {
        diff1=fabs(z1[i]-z0[i])/fabs(z0[i]);
        diff2=fabs(z2[i]-z0[i])/fabs(z0[i]);
        
        if(diff1>maxdiff) maxdiff=diff1;
        if(diff2>maxdiff) maxdiff=diff2;

        if(diff1>0.01 || diff2>0.01) printf("i %d: col: %f row: %f sol:%f \n",i,z1[i],z2[i],z0[i]);
    }
    printf("Error: %.2f%\n",maxdiff*100);

}

void convertXtoX1D_cols(double **X, double *X1D_cols, int N, int M)
{
    for(int i=0;i<N;i++){
        for(int j=0;j<M;j++){
            X1D_cols[INDEX_COL] = X[i][j]; 
        }
    }
}

void convertXtoX1D_rows(double **X, double *X1D_rows, int N, int M)
{
    for(int i=0;i<N;i++){
        for(int j=0;j<M;j++){
            X1D_rows[INDEX_ROW] = X[i][j]; 
        }
    }
}

void calc_z_colwise(double * __restrict__ z1, double* __restrict__ w, double b, double * __restrict__ X1D_cols,int N, int M )
{    
    #pragma acc parallel loop copyin(X1D_cols[0:N*M],w[M],b) copyout(z1[0:N])
    for(int i=0;i<N;i++){
        double sum = 0.0;
        #pragma acc loop reduction(+:sum)
        for(int j=0;j<M;j++){
            sum += X1D_cols[INDEX_COL] * w[j]; 
        }
        z1[i] = sum + b;
    }        
}

void calc_z_rowwise(double * __restrict__ z2, double* __restrict__ w, double b, double * __restrict__ X1D_rows,int N, int M)
{
    #pragma acc parallel loop copyin(X1D_rows[0:N*M],w[M],b) copyout(z2[0:N])
    for(int i=0;i<N;i++){
        double sum = 0.0;
        #pragma acc loop reduction(+:sum)
        for(int j=0;j<M;j++){
            sum += X1D_rows[INDEX_ROW] * w[j]; 
        }
        z2[i] = sum + b;
    }
}


void make_comparison(double *w, double b,  double **X, int N, int M)
{

    double* z1;
    double* z2;
    double* X1D_cols;
    double* X1D_rows;

    z1=(double*) malloc(N*sizeof(double));
    z2=(double*) malloc(N*sizeof(double));
    X1D_rows = (double*) malloc(N*M*sizeof(double));
    X1D_cols = (double*) malloc(N*M*sizeof(double));


    for(int i=0;i<N;i++){
        z1[i]=0.0;
        z2[i]=0.0;
    }

    //Convert X to 1D array
   
    
    convertXtoX1D_rows(X, X1D_rows, N, M );
    
    convertXtoX1D_cols(X, X1D_cols, N, M);

    //Calculate z= w^T*X + b
    calc_z_colwise(z1, w, b, X1D_cols, N, M);

    calc_z_rowwise(z2, w, b, X1D_rows, N, M); 

    //Verify solution
    verify(z1, z2,  N);

    free(z1);
    free(z2);
    free(X1D_rows);
    free(X1D_cols);
}


