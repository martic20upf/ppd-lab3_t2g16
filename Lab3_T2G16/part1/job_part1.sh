#!/bin/bash

#SBATCH --job-name=ex1
#SBATCH --output=ex1_%j.out
#SBATCH --error=ex1_%j.err
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --exclusive
#SBATCH --time=00:00:20

source /shared/profiles.d/easybuild.sh
module load PGI CUDA

make || exit 1      # Exit if make fails
./script.sh
