#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <sys/times.h>
#include "regression.h"

#define ind(i, j, m) (((i)*(m)) + (j))


double getTime() 
{
        struct timeval now;

        if (gettimeofday(&now, (struct timezone *) 0) != 0) {
            exit(1);
        }

        return(((double) now.tv_sec + (double) now.tv_usec / 1000000) +1.0);
}

void initialize_with_zeros(double *w, double &b, int M)
{
    for(int i=0;i<M;i++)
        w[i]=0.0;
    b=0.0;
}


/*Here starts the solution*/
// To Do: Copy the best solution from the exercise 1
void convertXtoX1D(double **X, double *X1D, int N, int M)
{
    for(int i=0;i<N;i++){
        for(int j=0;j<M;j++){
            X1D[j+i*M] = X[i][j]; 
        }
    }
}

void convertXtoX1D_dw(double **X, double *X1D_cols, int N, int M)
{
    for(int i=0;i<N;i++){
        for(int j=0;j<M;j++){
            X1D_cols[i+j*N] = X[i][j]; 
        }
    }
}


// To Do: Copy the best solution of the previous exercise
void calc_z(double * __restrict__ z, double* __restrict__ w, double b, double * __restrict__ X1D,int N, int M )
{
    #pragma acc parallel loop copyin(X1D[0:N*M],w[0:M]) copyout(z[0:N]) async(1)
    for(int i=0;i<N;i++){
        double sum = 0.0;    
        #pragma acc loop reduction(+:sum)
        for(int j=0;j<M;j++){
            sum += X1D[i*M+j] * w[j]; 
        }
        z[i] = sum + b;
    }
}

void sigmoid(double* __restrict__ z, double* __restrict__ yhat, int N) 
{
    #pragma acc parallel loop copyin(z[0:N]) copyout(yhat[0:N]) async(1)
    for(int i=0;i<N;i++){
        yhat[i]= 1.0/(1+exp(-z[i]));
    }
}

double calc_cost(double* __restrict__ y, double* __restrict__ yhat, int N)
{
    double cost = 0.0;
    #pragma acc parallel loop reduction (+:cost) copyin(y[0:N],yhat[0:N]) async(1)
    for(int i=0;i<N;i++){
        cost += y[i]*log(yhat[i]) + (1-y[i])*log(1-yhat[i]);
    } 
    cost = (-1.0/N)*cost;
    return cost;
}

void calc_dw(double* __restrict__ X1D_cols, double*__restrict__ y, double* __restrict__ yhat, double* __restrict__ dw, int N, int M)
{


    #pragma acc parallel loop copyout(dw[0:M]) copyin(y[0:N],yhat[0:N],X1D_cols[0:N*M]) async(1)
    for(int j=0;j<M;j++){
        double sum = 0.0; 
        #pragma acc loop reduction(+:sum)
        for(int i=0;i<N;i++){
            sum += X1D_cols[i+j*N] * (yhat[i]-y[i]);
        }
        dw[j] = sum/N;
    }
}

double calc_db(double* __restrict__ y, double* __restrict__ yhat,  int N)
{
    double auxdb=0.0;
    #pragma acc parallel loop copyin(yhat[0:N],y[0:N]) async(1)
    for(int i=0;i<N;i++){
        auxdb+=yhat[i]-y[i];
    }
    return auxdb/N;
}

void optimize(double *w, double b,  double **X, double *y, double *yhat, int N, int M)
{
    double cost=1000;
    double learning_rate=0.005;
    int num_its=101;

    double db; 
    double* dw  = (double *) malloc(M*sizeof(double));
    double* z   = (double *) malloc(N*sizeof(double));
    double* X1D = (double *) malloc(N*M*sizeof(double));
    double* X1D_cols = (double *) malloc(N*M*sizeof(double));

    double begin, end;

    initialize_with_zeros(w, b, M);

    //Copy the convertion of the best format
    convertXtoX1D(X, X1D, N, M );
    convertXtoX1D_dw(X, X1D_cols, N, M );
    begin = getTime();

    #pragma acc enter data copyin(X1D[0:N*M], X1D_cols[0:N*M],y[0:N],w[0:M]) create(z[0:N],yhat[0:N],dw[0:M]) 
    
    for(int it=0; it<num_its;it++){
       /*Forward propagation*/
  
       // Calculate z= w*X + b
       calc_z(z, w, b, X1D, N, M);

       // Estimation function yh= sigmoid(z)
       sigmoid(z, yhat, N);

       // Cost function
       cost=calc_cost(y, yhat, N);

       /*Back propagation*/

       //Calculate gradients
       calc_dw(X1D_cols, y, yhat, dw, N, M);

       db = calc_db(y, yhat, N);

	    //Correct w and b using gradients
        #pragma acc parallel loop copy(w[0:M]) copyin(dw[0:M]) async(1)
        for(int i=0;i<M;i++)
            w[i] = w[i] - learning_rate*dw[i];

        b = b - learning_rate * db;
    
        if(it%10==0){
            #pragma acc update device(X1D[0:N*M]) async(2) wait(2)
            printf("Cost after iteration %d : %f\n",it, cost);
        }
    }
    
    #pragma acc exit data delete(X1D[0:N*M],w[0:M],y[0:N],z[0:N],yhat[0:N],dw[0:M])

    end = getTime();

    printf("Time : %f\n",end-begin);

}


