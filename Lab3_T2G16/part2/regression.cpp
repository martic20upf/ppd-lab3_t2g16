#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <sys/times.h>
#include "regression.h"

#define ind(i, j, m) (((i)*(m)) + (j))


double getTime() 
{
        struct timeval now;

        if (gettimeofday(&now, (struct timezone *) 0) != 0) {
            exit(1);
        }

        return(((double) now.tv_sec + (double) now.tv_usec / 1000000) +1.0);
}

void initialize_with_zeros(double *w, double &b, int M)
{
    for(int i=0;i<M;i++)
        w[i]=0.0;
    b=0.0;
}


/*Here starts the solution*/
// To Do: Copy the best solution from the exercise 1
void convertXtoX1D(double **X, double *X1D, int N, int M)
{
    for(int i=0;i<N;i++){
        for(int j=0;j<M;j++){
            X1D[j+i*M] = X[i][j]; 
        }
    }
}


// To Do: Copy the best solution of the previous exercise
void calc_z(double * __restrict__ z, double* __restrict__ w, double b, double * __restrict__ X1D,int N, int M )
{
    for(int i=0;i<N;i++){
        double sum = 0.0;       
        for(int j=0;j<M;j++){
            sum += X1D[i*M+j] * w[j]; 
        }
        z[i] = sum + b;
    }
}

void sigmoid(double* __restrict__ z, double* __restrict__ yhat, int N) 
{
    for(int i=0;i<N;i++){
        yhat[i]= 1.0/(1+exp(-z[i]));
    }
}

double calc_cost(double* __restrict__ y, double* __restrict__ yhat, int N)
{
    double cost=0.0;
   
    for(int i=0;i<N;i++){
        cost += y[i]*log(yhat[i]) + (1-y[i])*log(1-yhat[i]);
    } 
    cost = (-1.0/N)*cost;
    return cost;
}

void calc_dw(double* __restrict__ X1D, double*__restrict__ y, double* __restrict__ yhat, double* __restrict__ dw, int N, int M)
{
    for(int j=0;j<M;j++){
        double sum = 0.0;
        for(int i=0;i<N;i++){
            sum += X1D[i*M+j] * (yhat[i]-y[i]);
        }
        dw[j] = sum/N;
    }
}



double calc_db(double* __restrict__ y, double* __restrict__ yhat,  int N)
{
    double auxdb=0.0;
    for(int i=0;i<N;i++){
        auxdb+=yhat[i]-y[i];
    }
    return auxdb/N;
}

void optimize(double *w, double b,  double **X, double *y, double *yhat, int N, int M)
{
    double cost=1000;
    double learning_rate=0.005;
    int num_its=101;

    double db; 
    double* dw  = (double *) malloc(M*sizeof(double));
    double* z   = (double *) malloc(N*sizeof(double));
    double* X1D = (double *) malloc(N*M*sizeof(double));

    double begin, end;

    initialize_with_zeros(w, b, M);

    //Copy the convertion of the best format
    convertXtoX1D(X, X1D, N, M );
  
    begin = getTime();
    for(int it=0; it<num_its;it++){

       /*Forward propagation*/
  
       // Calculate z= w*X + b
       calc_z(z, w, b, X1D, N, M);
       
       // Estimation function yh= sigmoid(z)
       sigmoid(z, yhat, N);

       // Cost function
       cost=calc_cost(y, yhat, N);
  
       /*Back propagation*/

       //Calculate gradients
       calc_dw(X1D, y, yhat, dw, N, M);

       db = calc_db(y, yhat, N);

	//Correct w and b using gradients
        for(int i=0;i<M;i++)
            w[i] = w[i] - learning_rate*dw[i];

        b = b - learning_rate * db;

        if(it%10==0)
            printf("Cost after iteration %d : %f\n",it, cost);
    }
    end = getTime();

    printf("Time : %f\n",end-begin);

}


