#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "vecmatmult.h"

// To Do: Copy your macro from exercise 1 or create a more adeaquate
#define INDEX_ROW j+i*M

void initialize_test(double *w, double &b, int N, int M){

    for(int i = 0; i < M; i++)
        w[i]=sin(1.0*i) + sin(5.0*i) + sin(10.0*i);
    b=0.5;
}

void verify(double *z1, int N)
{
    double diff1,maxdiff=0.0;
  
    double *z0;

    z0= (double *) malloc( N * sizeof(double));

    FILE *fp;
    fp=fopen("../data/sol.dat","r");
    
    for(int j=0;j<N;j++)
	fscanf(fp," %lf \n",&z0[j]);

    fclose(fp);

    for(int i=0;i<N;i++)
    {
        diff1=fabs(z1[i]-z0[i])/fabs(z0[i]);
        
        if(diff1>maxdiff) maxdiff=diff1;

        // if(diff1>0.01) printf("i %d: cuda: %f sol:%f \n",i,z1[i],z0[i]);
    }
    printf("Error: %f%\n",maxdiff*100);

}



// Copy your convertion from exercise 1 or create a new one more adeaquate to your implementation
void convertXtoX1D(double *X1D, double **X, int N, int  M, int threads)
{
    // To Do:
    //rows implementation
    for(int i=0;i<N;i++){
        for(int j=0;j<M;j++){
            X1D[j+i*M] = X[i][j]; 
        }
    }
}


/* CUDA kernel
Remeber some things that help the implementation:
 - The number of threads is a power of  2
 - M is divisible by the number of threads requested 
 - Take a look at the compilation info provided by PGI in exercise 1 */
__global__ void cucalc_z_mul(int n,int m, double* d_w, double* d_z, double* d_X1D)
{
    int index = threadIdx.x + blockIdx.x*blockDim.x;
    for(int i=0;i<n;i++){

        // double sum = 0.0;
        // for(int j=0;j<M;j++){
        //     sum += d_X1D[INDEX_COL] * w[j]; 
        // }
        d_X1D[i*m + index] *= d_w[index];

        // z1[i] = sum + b;
      
        __syncthreads();

        int block_2 = blockDim.x/2;
        for (unsigned int s=block_2; s>0; s>>=1) {
            if (threadIdx.x < s) {
                d_X1D[i*m + index] += d_X1D[i*m + index + s];
            }
            __syncthreads();
        }
    }
}

__global__ void cucalc_z_sum(int n,int m, double* d_z, double* d_X1D, double b, int threads)
{
    int index = threadIdx.x + blockIdx.x*blockDim.x;
    if(index<n){
        for(int i=0;i<m;i+=threads){ 
            d_z[index] += d_X1D[index*m + i];
        }
        d_z[index] += b;
    }

}

void calc_z_cuda(double * __restrict__ z1, double* __restrict__ w, double* __restrict__ X1D, double b, int N, int M){

    //To Do:

    //arrays to be used by cuda kernel 
    double *d_w;
    double *d_z;
    double *d_X1D;


    //To Do: Allocate arrays in GPU
    cudaMalloc((void**) &d_w, M*sizeof(double));
    cudaMalloc((void**) &d_z, N*sizeof(double));
    cudaMalloc((void**) &d_X1D, M*N*sizeof(double));
    
    //To Do: Transfer data to GPU
    cudaMemcpy(d_X1D, X1D, N*M*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_w, w, M*sizeof(double), cudaMemcpyHostToDevice);

    //To Do: Calculate threads and blocks
    int threads;
    int blocks;
    threads=128;
    blocks= M/threads;


    //Create the launching grids, you can change this to try a more complex configuration 
    dim3 gridBlocks(blocks,1,1);
    dim3 gridThreads(threads,1,1);


    //To Do: Execute in GPU.  You can change this line if you want to try a more complex launch
    cucalc_z_mul<<<gridBlocks,gridThreads>>>(N, M, d_w, d_z, d_X1D);


    // blocks= N/threads;
    // if(N%threads!=0) blocks++;
    // dim3 gridBlocks(blocks,1,1);
    // dim3 gridThreads(threads,1,1);
    cucalc_z_sum<<<gridBlocks,gridThreads>>>(N, M, d_z, d_X1D, b, threads);

    //To Do: Transfer result back
    cudaMemcpy(z1, d_z, N*sizeof(double), cudaMemcpyDeviceToHost);
    
    //Free arrays in GPU
    cudaFree(d_w);
    cudaFree(d_z);
    cudaFree(d_X1D);

    // printf("3Error: %s\n", cudaGetErrorString(cudaGetLastError()));
}



void make_comparison(double *w, double b,  double **X, int N, int M)
{

    double* z1;

    z1=(double*) malloc(N*sizeof(double));

    double *X1D;
    X1D = (double*) malloc(N*M*sizeof(double));
 
    convertXtoX1D(X1D,X,N,M,128);

    calc_z_cuda(z1, w, X1D, b, N, M);

    verify(z1, N);


    free(z1);
    free(X1D);
}

