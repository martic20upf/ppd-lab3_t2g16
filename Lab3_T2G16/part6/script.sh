#!/bin/bash
nvprof --print-gpu-summary ./vecmatmult  2> time.dat
echo " "
echo "Times: "
grep -o -P '.{0,11}cucalc_z' time.dat 
rm time.dat
