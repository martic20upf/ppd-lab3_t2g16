#!/bin/bash

#SBATCH --job-name=ex3
#SBATCH --output=multicore_%j.out
#SBATCH --error=multicore_%j.err
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --exclusive
#SBATCH --time=00:00:20

source /shared/profiles.d/easybuild.sh
module load PGI CUDA

./script_multicore.sh || exit 1      # Exit if make fails
./regre-multicore
