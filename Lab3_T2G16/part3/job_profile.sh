#!/bin/bash

#SBATCH --job-name=ex3
#SBATCH --output=screen_%j.out
#SBATCH --error=profile_%j.err
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --exclusive
#SBATCH --time=00:00:30

source /shared/profiles.d/easybuild.sh
module load PGI CUDA

make || exit 1      # Exit if make fails
nvprof --print-gpu-summary ./regression

