#!/bin/bash

#SBATCH --job-name=ex4
#SBATCH --output=screen_%j.out
#SBATCH --error=compile_%j.err
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --exclusive
#SBATCH --time=00:00:30

source /shared/profiles.d/easybuild.sh
module load PGI CUDA

make || exit 1      # Exit if make fails
./regression

